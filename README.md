# Simple Instance

Simple Instance is a self hosted PHP version of a social network front end that uses Hey.Café as the data source.
The implementation can be just a mirror service with modifications or can be set to a specific Café for content source creating a private network.

## Structure

The code is easy to edit and as such most features will be grouped into functions so you can modify pages and how it works but still run updates.
Updates will only update if you wish the /liquid_functions folder so that important functions to the service are updated but your modifications in other files remain.

Files outside folders listed as solid should be updated on new updates but may break modifications if not within liquid folders.
Our core team will do our best to add changes that are needed mostly to files within the liquid folders.

/functions_core - Functions we use to create and do actions on pages like conversation_create().
/javascript_core - Core JS functions that also include code that is downloaded from the main Hey.Café source (https://hey.cafe/application/) for things like formatting.

/storage_cache - Cache will store temp data needed for things like media uploads and stuff we don't want updates to break.
/storage_processing - Changes in a que to be uploaded or made to the main service when things fail like posts to post later.
/storage_replication - Stores data that will be used for when the main service cant be reached.

## Getting started

Just upload this project to your hosting service and modify /config.json with your details.