<?php
$setupstep=false;

$content_body.="<heading>Setup</heading>";

//##############################################################
//##############################################################-- Auto Processes
//##############################################################

//--Setup update key
if ($system_config["admin_update_key"]==false){
	$system_config["admin_update_key"]=generate_random_code(30);
	$system_config_update=true;
}

//##############################################################
//##############################################################-- Setup Admin Account and Key
//##############################################################
if ($setupstep==false){
	if ($system_config["admin_apikey"]==false){
		$setupstep=true;
		$content_body.="<bubble>";
		$content_body.="<bubble class='warning'>We need you to act as the admin for this community, to get started we need an active admin API key for Hey.Café. You can do this by clicking the box below to go to the next step.</bubble>";
		$content_body.="<BR><BR><div id='heycafe_scriptbox'></div><action type='heycafe_sign_in_with_api' element='heycafe_scriptbox' publisher='".$system_config["meta_title"]."' reason='For setup and administration of self-hosted instance' url='https://".$system_domain."/setup' title='Setup API Key using Hey.Café'></action>";
		$content_body.="</bubble>";
		
		if (isset($_GET["loginkey"])){
			$loginkey=makesafe($_GET["loginkey"]);
			
			$source_info_request=fetchurl("https://endpoint.hey.cafe/api/account_loginkey?query=".$loginkey."");
			if ($source_info_request!=false){
				$source_info=json_decode($source_info_request,true);
				if ($source_info["system_api_error"]!="notvalid"){
					if (isset($source_info["response_data"]["id"])){
						if ($source_info["response_data"]["auth_domain"]==$system_domain){
							$system_config["admin_apikey"]=$loginkey;
							$system_config["admin_id"]=$source_info["response_data"]["id"];
							$system_config_update=true;
							$content_body.="<action type='alert'>Thank you for the key ".$source_info["response_data"]["name"]."!</action>";
							$content_body.="<action type='redirect' url='/setup'></action>";
						}
					}else{
						$content_body.="<bubble class='error'>The key failed</bubble>";
					}
				}else{
					$content_body.="<bubble class='error'>Key is not valid</bubble>";
				}
			}
		}
	}
}

//##############################################################
//##############################################################-- Café Source Select
//##############################################################
if ($setupstep==false){
	if ($system_config["cafe_source"]==false){
		$setupstep=true;
		$content_body.="<bubble>";
		$content_body.="<bubble class='warning'>We just need to pick the café you are in to act as the data source for the instance.</bubble>";
		$content_body.="<BR><BR>";
		
		$cafes=heycafe_api_request_admin("get_account_cafes",['query'=>$system_config["admin_id"],'count'=>200,'manage'=>'true']);
		if ($cafes["system_api_error"]==false){
			foreach ($cafes["response_data"]["cafes"] as $cafe){
				if (isset($_GET["cafe"])){
					$id=makesafe($_GET["cafe"]);
					if ($id==$cafe["id"]){
						$system_config["cafe_source"]=$id;
						$system_config["meta_title"]=$cafe["name"];
						$system_config["meta_description"]=$cafe["bio"];
						$system_config["meta_image"]=$cafe["header"];
						$system_config["meta_icon"]=$cafe["avatar"];
						$system_config_update=true;
						
						$content_body.="<action type='alert'>Setup done, now login with Hey.Café</action>";
						$content_body.="<action type='redirect' url='/login'></action>";
					}
				}
				
				$content_body.="<a href='/setup?cafe=".$cafe["id"]."'><bubble>";
				$content_body.="<img src='".$cafe["avatar"]."' style='float:left;width:45px;height:45px;margin-right:10px;border-radius:100%;'><strong>".$cafe["name"]."</strong> · !".$cafe["alias"]."";
				$content_body.="<div>".$cafe["bio"]."</div>";
				$content_body.="</bubble></a>";
			}
		}else{
			$content_body.="<bubble class='error'>You have no cafés you moderate</bubble>";
		}
		
		$content_body.="</bubble>";
	}
}

//##############################################################
//##############################################################-- Final Setup Area
//##############################################################
if ($setupstep==false){
	if (heycafe_is_system_admin(true)==true){
		$content_body.="<bubble>";
		$content_body.="<p>Your instance is all setup, you can <a href='/update'>click here to run updates</a>. You can see moderator tools by logging into <a href='https://admin.hey.cafe'>admin.hey.cafe</a>.</p>";
		
		//#######################
		//#######################-- Metadata
		//#######################
		$content_body.="<hr></hr>";
		
		$content_body.="<bubble>";
		$content_body.="<img src='".$system_config["meta_icon"]."' style='float:left;margin-right:15px;border-radius:13px;width:80px;height:80px;'>";
		$content_body.="<strong>".$system_config["meta_title"]."</strong>";
		$content_body.="<div>".$system_config["meta_description"]."</div>";
		$content_body.="</bubble>";
		
		//#######################
		//#######################-- Café - Auto Join
		//#######################
		$content_body.="<hr></hr>";
		$content_body.="<subheading>Café - Auto Join</subheading><p>Auto join uses the admin account to approve all new users to a café that is set to private so they can use the community.</p>";
		if ($system_config["cafe_rule_autojoin"]==false){
			$content_body.="<h3><a href='/setup?cafe_rule_autojoin=true'>".get_bootstrap_icon("toggle-off")." Off</a></h3>";
		}else{
			$content_body.="<h3><a href='/setup?cafe_rule_autojoin=false'>".get_bootstrap_icon("toggle-on")." On</a></h3>";
		}
		if (isset($_GET["cafe_rule_autojoin"])){
			$value=makesafe($_GET["cafe_rule_autojoin"]);
			if ($value=="true"){
				$system_config["cafe_rule_autojoin"]=true;
				$system_config_update=true;
			}else{
				$system_config["cafe_rule_autojoin"]=false;
				$system_config_update=true;
			}
			$content_body.="<action type='alert'>Setting Updated</action>";
			$content_body.="<action type='redirect' url='/setup'></action>";
		}
		
		//#######################
		//#######################-- Café - Use Meta
		//#######################
		$content_body.="<hr></hr>";
		$content_body.="<subheading>Café - Use Meta</subheading><p>Use the metadata from the café for the site title, description, icon and main photo (header). This meta data will only be updated when you run updates. You can setup updates to run using a CRON job on the url <code>https://".$system_domain."/update?code=".$system_config["admin_update_key"]."</code>.</p>";
		if ($system_config["cafe_rule_meta"]==false){
			$content_body.="<h3><a href='/setup?cafe_rule_meta=true'>".get_bootstrap_icon("toggle-off")." Off</a></h3>";
		}else{
			$content_body.="<h3><a href='/setup?cafe_rule_meta=false'>".get_bootstrap_icon("toggle-on")." On</a></h3>";
		}
		if (isset($_GET["cafe_rule_meta"])){
			$value=makesafe($_GET["cafe_rule_meta"]);
			if ($value=="true"){
				$system_config["cafe_rule_meta"]=true;
				$system_config_update=true;
			}else{
				$system_config["cafe_rule_meta"]=false;
				$system_config_update=true;
			}
			$content_body.="<action type='alert'>Setting Updated</action>";
			$content_body.="<action type='redirect' url='/setup'></action>";
		}
		
		$content_body.="</bubble>";
	}
}