<?php
$alias=trim(str_replace("/profile", "", $system_uri),"/");

$account=heycafe_api_request("get_account_info",['query'=>$alias]);
if ($account["system_api_error"]==false){
	if (heycafe_account_check_in_cafe($account["response_data"]["id"],$system_config["cafe_source"])){
		$meta_image=$account["response_data"]["avatar"];
		
		$content_body.="<bubble>";
		
		$content_body.="<div class='profile_background' style='border-radius: 15px;overflow: hidden;margin-left: -15px;margin-right: -15px;margin-top: -15px !important;border-bottom-left-radius:0px;border-bottom-right-radius:0px;'><div class='optimizer_bg aspectcase twentyonebynine' id='js_replace_background' source='".$account["response_data"]["header"]."' style=\"background-image: url('".$system_blank_image."');\"><div class='content'>";
		$content_body.="</div></div></div>";
		$content_body.="<img class='avatar optimizer_img' style='width:150px;height:150px;margin-top: -75px;margin-left: 15px;border-radius:100%;z-index: 5;position: relative;float: left;margin-right: 15px;' alt='Profile image' source='".$account["response_data"]["avatar"]."' src='".$system_blank_image."'>";
		
		$content_body.="<subheading>".$account["response_data"]["name"]."</subheading>";
		$content_body.="<div>".$account["response_data"]["bio"]."</div>";
		$content_body.="<hr></hr>";
		
		
		$content_body.="</bubble>";
		
	}else{
		$content_body.="<bubble class='error'>Cant find account with the alias @".$alias."</bubble>";
	}
}else{
	$content_body.="<bubble class='error'>Cant find account with the alias @".$alias."</bubble>";
}