<?php

$content_body.="<heading>Login</heading>";
$content_body.="<bubble><bubble class='warning'>You can login using your Hey.Café account to get access to the community.</bubble><BR><BR><div id='heycafe_scriptbox'></div><action type='heycafe_sign_in_with_api' element='heycafe_scriptbox' publisher='".$system_config["meta_title"]."' reason='Access to the community' url='https://".$system_domain."/login' title='Login using Hey.Café'></action></bubble>";

if (isset($_GET["loginkey"])){
	$loginkey=makesafe($_GET["loginkey"]);
	
	$source_info_request=fetchurl("https://endpoint.hey.cafe/api/account_loginkey?query=".$loginkey."");
	if ($source_info_request!=false){
		$source_info=json_decode($source_info_request,true);
		if ($source_info["system_api_error"]!="notvalid"){
			if (isset($source_info["response_data"]["id"])){
				if ($source_info["response_data"]["auth_domain"]==$system_domain){
					$_SESSION["account_key"]=$loginkey;
					$account_key=$loginkey;
					$account_id=$source_info["response_data"]["id"];
					$account_name=$source_info["response_data"]["name"];
					$account_avatar=$source_info["response_data"]["avatar"];
					$account_header=$source_info["response_data"]["header"];
					
					//--If not in cafe do the auto request now
					$needsjoin=false;
					$needsaproval=false;
					
					//--Check status
					$cafe=heycafe_api_request("get_account_cafes",['query'=>$account_id, 'cafe'=>$system_config["cafe_source"]]);
					if ($cafe["system_api_error"]==false){
						if (is_array($cafe["response_data"]["cafes"])){
							if ($cafe["response_data"]["cafes"]["".$system_config["cafe_source"].""]["status"]=="waiting"){
								$needsaproval=true;
							}
						}else{
							$needsjoin=true;
						}
					}
					
					//--Join
					if ($needsjoin==true){
						$status=heycafe_api_request("post_cafe_join",['cafe'=>$system_config["cafe_source"]]);
						if ($status["system_api_error"]==false){
							if ($status["response_data"]=="wait"){
								$needsaproval=true;
							}
						}else{
							$needsaproval=true;
						}
					}
					
					//--Approve
					if ($needsaproval==true){
						if ($system_config["cafe_rule_autojoin"]==true){
							$approve=heycafe_api_request_admin("post_cafe_update_member_status",['cafe'=>$system_config["cafe_source"],'member'=>$account_id,'mode'=>'active']);
						}
					}
					
					$content_body.="<action type='alert'>Welcome ".$account_name."!</action>";
					$content_body.="<action type='redirect' url='/'></action>";
				}
			}else{
				$content_body.="<bubble class='error'>The login failed</bubble>";
			}
		}else{
			$content_body.="<bubble class='error'>Key is not valid</bubble>";
		}
	}
}