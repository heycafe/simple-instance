<?php
$allowedtorun=false;

if ($account_id==$system_config["admin_id"]){
	$allowedtorun=true;
}

if (isset($_GET["code"])){
	$code=makesafe($_GET["code"]);
	
	if ($code==$system_config["admin_update_key"]){
		$allowedtorun=true;
	}
}

if ($allowedtorun==true){
	$content_body.="<heading>Update Instance</heading>";
	$content_body.="<bubble>";
	$content_body.="<p>We are updating this instance and some data, it might take a moment...</p>";
	
	
	//##############################################################
	//##############################################################-- Update Meta Data
	//##############################################################
	
	$content_body.="<hr></hr>";
	$content_body.="<subheading>Café Metadata</subheading>";
	
	if ($system_config["cafe_rule_meta"]==true){
		$cafe=heycafe_api_request_admin("get_cafe_info",array('query'=>$system_config["cafe_source"]));
		if ($cafe["system_api_error"]==false){
			$system_config["meta_title"]=$cafe["response_data"]["name"];
			$system_config["meta_description"]=$cafe["response_data"]["bio"];
			$system_config["meta_image"]=$cafe["response_data"]["header"];
			$system_config["meta_icon"]=$cafe["response_data"]["avatar"];
			$system_config_update=true;
			$content_body.="<bubble class='good'>Metadata for the Café has been updated</bubble>";
		}else{
			$content_body.="<bubble class='error'>Got the error ".$cafe["system_api_error"]." when trying to update metadata</bubble>";
		}
	}else{
		$content_body.="<bubble class='warning'>Metadata update is turned off</bubble>";
	}
	
	//##############################################################
	//##############################################################-- Update Install
	//##############################################################
	
	$content_body.="<hr></hr>";
	$content_body.="<subheading>Instance Update Install</subheading>";
	
	$downloadworked=true;
	$source = urldecode("https://gitlab.com/heycafe/simple-instance/-/archive/main/simple-instance-main.zip?version=".time()."");
	$dest = "update-package.zip";
	
	$options  = array('http' => array('user_agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.2 Safari/605.1.15'));
	$context  = stream_context_create($options);
	
	if ($downloadworked==true){
		$response = file_get_contents($source, false, $context);
		if ($response === false){
			$downloadworked=false;
			$content_body.="<bubble class='error'>The update file download failed</bubble>";
		}
	}
	
	if ($downloadworked==true){
		$save = file_put_contents($dest, $response);
		if ($save === false){
			$downloadworked=false;
			$content_body.="<bubble class='error'>The update file download failed</bubble>";
		}
	}
	
	if ($downloadworked==true){
		unlink("script_combined_compressed.js");
		//--unlink(".htaccess");
		$files_leftath = 'update-package.zip';
		$zip = new ZipArchive;
		$files_on=0;
		$files_left=500;
		if ($zip->open($files_leftath) === true){
			$files=$zip->numFiles;
			while ($files_left >= 1){
				$originalfilename = $zip->getNameIndex($files_on);
				if ($originalfilename!=""){
					$fileinfo = pathinfo($originalfilename);
					$filename = substr($originalfilename, strpos($originalfilename, "/") + 1);
					
					$move=true;
					if ($filename=="settings.hcjson"){ $move=false; }
					if ($filename==".nova/"){ $move=false; }
					if ($filename==".nova/Configuration.json"){ $move=false; }
					if ($filename=="functions_extra/"){ $move=false; }
					if ($filename=="javascript_extra/"){ $move=false; }
					if ($filename=="storage_cache/"){ $move=false; }
					if ($filename=="storage_processing/"){ $move=false; }
					if ($filename=="storage_replication/"){ $move=false; }
					
					//--Config update!
					if ($filename=="settings.hcjson"){
						$source=file_get_contents("zip://".$files_leftath."#".$originalfilename."");
						if ($source!=""){
							$source=json_decode($source,true);
							foreach ($source as $index => $value){
								if (isset($system_config["".$index.""])){
									$content_body.="<div>Config file already has ".$index."</div>";
								}else{
									$system_config["".$index.""]=$value;
									$content_body.="<div>The config value for ".$index." has been added to the config file</div>";
									$system_config_update=true;
								}
							}
							
						}else{
							$content_body.="<div><strong>Error!</strong> Cant read config data</div>";
						}
					}
					
					if ($move==true){
						if ($filename!=""){
							if (file_exists($filename)){
								if (!is_dir($filename)){
									unlink($filename);
								}
							}
							
							if(substr($filename , -1)!=='/'){
								copy("zip://".$files_leftath."#".$originalfilename, $filename);
								$content_body.="<div>File ".$filename." updated</div>";
							}else{
								mkdir($filename, 0755, true);
								$content_body.="<div>Made folder ".$filename."</div>";
							}
						}
					}else{
						if (file_exists($filename)){
							$content_body.="<div>File ".$filename." skiped - we dont replace this file</div>";
						}
					}
					$files_left=$files_left-1;
					$files_on=$files_on+1;
				}else{
					$files_left=0;
					$content_body.="<BR><BR><div>Files are all updated.</div>";
				}
			}
			$zip->close();
			unlink($files_leftath);
		}
	}
	
	if (!isset($_GET["code"])){
		$content_body.="<action type='alert'>Updates Done</action>";
		$content_body.="<action type='redirect_delay' url='/setup'></action>";
	}
	
	$content_body.="</bubble>";
}else{
	$content_body.="<action type='alert'>You do not have access to this page</action>";
	$content_body.="<action type='redirect' url='/setup'></action>";
}