<?php
if (heycafe_is_system_loggedin(true)==true){
	
	$content_body.="<heading>Feed</heading>";
	
	$feed=heycafe_api_request("get_feed_conversations",['cafe'=>$system_config["cafe_source"]]);
	if ($feed["system_api_error"]==false){
		
		$content_body.=json_encode($feed["response_data"]);
		
	}else{
		if ($feed["system_api_error"]=="not_cafe_member"){
			$content_body.="<bubble class='error'>Your membership has not been approved yet</bubble>";
		}else{
			$content_body.="<bubble class='error'>Error loading feed</bubble>";
		}
	}
	
}