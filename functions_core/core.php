<?php

//##############################################################
//##############################################################-- makesafe
//##############################################################

function makesafe($d,$type="basic"){
	$d = str_replace("\t","~~",$d);
	$d = str_replace("\r","",$d);
	$d = str_replace("\n","  ",$d);
	$d = str_replace("|","&#124;",$d);
	$d = str_replace("\\","&#92;",$d);
	$d = str_replace("(c)","&#169;",$d);
	$d = str_replace("(r)","&#174;",$d);
	$d = str_replace("\"","&#34;",$d);
	$d = str_replace("'","&#39;",$d);
	$d = str_replace("<","&#60;",$d);
	$d = str_replace(">","&#62;",$d);
	$d = str_replace("`","&#96;",$d);
	return $d;
}

//##############################################################
//##############################################################-- makesafe_delete
//##############################################################

function makesafe_delete($d){
	$d = str_replace("\t","",$d);
	$d = str_replace("\r","",$d);
	$d = str_replace("\n","",$d);
	$d = str_replace("|","",$d);
	$d = str_replace("\\","",$d);
	$d = str_replace("(c)","",$d);
	$d = str_replace("(r)","",$d);
	$d = str_replace("\"","",$d);
	$d = str_replace("'","",$d);
	$d = str_replace("<","",$d);
	$d = str_replace(">","",$d);
	$d = str_replace("`","",$d);
	return $d;
}

//##############################################################
//##############################################################-- get_percentage
//##############################################################

function get_percentage($total, $number){
	if ( $total > 0 ){
		return round($number / ($total / 100),2);
	}else{
		return 0;
	}
}

//##############################################################
//##############################################################-- get_true_userip
//##############################################################

function get_true_userip(){
	$ip="0.0.0.0";
	if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])){ if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; }}}
	if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])){ if (filter_var($_SERVER['HTTP_CF_CONNECTING_IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_CF_CONNECTING_IP']; }}}
	if (isset($_SERVER['Cf-Connecting-IP'])){ if (filter_var($_SERVER['Cf-Connecting-IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['Cf-Connecting-IP']; }}}
	if (isset($_SERVER['HTTP_CLIENT_IP'])){ if (filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_CLIENT_IP']; }}}
	if (isset($_SERVER['REMOTE_ADDR'])){ if (filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['REMOTE_ADDR']; }}}
	return $ip;
}

//##############################################################
//##############################################################-- get_bootstrap_icon
//##############################################################

function get_bootstrap_icon($name){
  return "<img alt='".$name."' class='svgicon' src='https://assets.heycafecdn.com/bootstrap_icons/svg/".$name.".svg' style='width: 1.2em;margin-left: 2px;margin-right: 2px;display: inline;vertical-align: text-top;'>";
}

//##############################################################
//##############################################################-- fetchurl
//##############################################################

function fetchurl($url){
	global $system_useragent;
	$process = curl_init($url);
	curl_setopt($process, CURLOPT_HEADER, 0);
	curl_setopt($process, CURLOPT_TIMEOUT, 30);
	curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($process, CURLOPT_USERAGENT, $system_useragent);
	curl_setopt($process, CURLOPT_FAILONERROR, true);
	curl_setopt($process, CURLOPT_AUTOREFERER, true);
	curl_setopt($process, CURLOPT_MAXREDIRS, 7);
	
	$return = curl_exec($process);
	if (curl_error($process)){
		return false;
	}
	curl_close($process);
	return $return;
}

//##############################################################
//##############################################################-- fetchurl_savefile
//##############################################################

function fetchurl_savefile($url,$file){
	global $system_useragent;
	$process = curl_init($url);
	curl_setopt($process, CURLOPT_HEADER, 0);
	curl_setopt($process, CURLOPT_TIMEOUT, 30);
	curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($process, CURLOPT_USERAGENT, $system_useragent);
	curl_setopt($process, CURLOPT_FAILONERROR, true);
	curl_setopt($process, CURLOPT_AUTOREFERER, true);
	curl_setopt($process, CURLOPT_MAXREDIRS, 7);
	
	$return = curl_exec($process);
	if (curl_error($process)){
		return false;
	}
	curl_close($process);
	
	$file_mini = fopen($file,"w");
	fwrite($file_mini,$return);
	fclose($file_mini);
}

//##############################################################
//##############################################################-- compress
//##############################################################

function compress($str){
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=preg_replace('/(?<!\S)\/\/\s*[^\r\n]*/', '', $str);
	$str=preg_replace('/((?=^)(\s*))|((\s*)(?>$))/', '', $str);
	$str=preg_replace('/[\t]+/', '', trim($str));
	$str=preg_replace('/(?<!\S)\/\/\--s*[^\r\n]*/', '', $str);
	$str=preg_replace('/(?<!\S)\/\/\##s*[^\r\n]*/', '', $str);
	$str=preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $str);
	$str=preg_replace("/\n /", "\n", $str);
	$str=preg_replace("/}\nvar/", "} var", $str);
	$str=preg_replace("/}\nsetTimeout/", "} setTimeout", $str);
	$str=preg_replace("/{\nsetTimeout/", "{ setTimeout", $str);
	$str=preg_replace("/\nvar/", " var", $str);
	$str=preg_replace("/\rvar/", " var", $str);
	$str=preg_replace("/\n var/", " var", $str);
	$str=preg_replace("/\r var/", " var", $str);
	$str=preg_replace("/\nif/", " if", $str);
	$str=preg_replace("/\rif/", " if", $str);
	$str=preg_replace("/\n if/", " if", $str);
	$str=preg_replace("/\r if/", " if", $str);
	$str=preg_replace("/\nelse/", " else", $str);
	$str=preg_replace("/\nwhile/", " while", $str);
	$str=preg_replace("/\rwhile/", " while", $str);
	$str=preg_replace("/;\n/", "; ", $str);
	$str=preg_replace("/{\n/", "{ ", $str);
	$str=preg_replace("/\n {/", "{ ", $str);
	$str=preg_replace("/\n{/", "{ ", $str);
	$str=preg_replace("/\nelse{/", "else{", $str);
	$str=preg_replace("/\nelse {/", "else{", $str);
	$str=preg_replace("/; \n/", "; ", $str);
	$str=preg_replace("/{ \n/", "{ ", $str);
	$str=preg_replace("/\n}/", " }", $str);
	$str=preg_replace("/\n }/", " }", $str);
	$str=preg_replace("/\r}/", " }", $str);
	$str=preg_replace("/\r }/", " }", $str);
	$str=preg_replace("/\nreturn/", " return", $str);
	$str=preg_replace("/}\nreturn/", "} return", $str);
	
	$str=preg_replace("/}\nfunction/", "} function", $str);
	$str=preg_replace("/}\rfunction/", "} function", $str);
	$str=preg_replace("/\n\\\$/", " $", $str);
	$str=preg_replace("/\ndocument./", " document.", $str);
	
	$str=str_replace("//--#############################################################", "", $str);

	//Short Fixes
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("} else {", "}else{", $str);
	$str=str_replace(") {", "){", $str);
	$str=str_replace("; }", ";}", $str);
	$str=str_replace("\" + ", "\"+", $str);
	$str=str_replace(" + \"", "+\"", $str);
	$str=str_replace("' + ", "'+", $str);
	$str=str_replace(" + '", "+'", $str);
	$str=str_replace("} }", "}}", $str);
	$str=str_replace(" == ", "==", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	
	//--Additions to compression with replacing function calls with shorter calls
	$str=str_replace("document.getElementById(", "aa(", $str);
	$str='function aa(s) { return document.getElementById(s); } '.$str.'';
	$str=str_replace("document.getElementsByTagName(", "ab(", $str);
	$str='function ab(s) { return document.getElementsByTagName(s); } '.$str.'';
	$str=str_replace("document.getElementsByClassName(", "ac(", $str);
	$str='function ac(s) { return document.getElementsByClassName(s); } '.$str.'';
	$str=str_replace("document.querySelector(", "ad(", $str);
	$str='function ad(s) { return document.querySelector(s); } '.$str.'';
	$str=str_replace("encodeURIComponent(", "ae(", $str);
	$str='function ae(s) { return encodeURIComponent(s); } '.$str.'';
	
	return $str;
}