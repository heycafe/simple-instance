<?php

//##############################################################
//##############################################################-- heycafe_formatting_standard
//##############################################################

function heycafe_formatting_standard($preview){
	$preview = str_replace("&#124;", "|", $preview);
	$preview = str_replace("&#96;", "`", $preview);
	$preview = str_replace("&#39;", "'", $preview);
	
	$preview = str_replace("    ", "</p><p>", $preview);
	$preview = str_replace("  ", "<BR>", $preview);
	$preview = str_replace("\<p\>	", "<p>", $preview);
	$preview = str_replace("\<p\> ", "<p>", $preview);
	$preview = str_replace("~~", "\t", $preview);
	
	//--Links
	$check_hash = preg_match_all("/\bhttps?:\/\/([^\<\n\s]+)\b/", $preview, $convofind);
	foreach ($convofind[0] as $ht){
		if (str_ends_with($ht, '.jpeg')==false AND str_ends_with($ht, '.jpg')==false AND str_ends_with($ht, '.png')==false AND str_ends_with($ht, '.gif')==false) {
			$preview=str_replace($ht,"<a href='".$ht."'>".$ht."</a>",$preview);
		}else{
			$preview=str_replace($ht,"<img src='https://proxy.hey.cafe/ext_image/gif/?url=".urlencode($ht)."&quality=raw' style='margin-top:15px;max-width:100%;border-radius:9px;'>",$preview);
		}
	}
	
	//--Match users
	$check_hash = preg_match_all("/(\@)([^\n\s\!\?\@\#\$\%\&\.\,\<\>\(\)]+)/", $preview, $convofind);
	foreach ($convofind[2] as $ht){
		if ($ht!="everyone"){
			$preview=str_replace("@".$ht."","<a href='https://hey.cafe/@".$ht."'>".$ht."</a>", $preview);
		}
	}
	
	//--Match cafes
	$check_hash = preg_match_all("/(\!)([^\n\s\!\?\@\#\$\%\&\.\,\<\>\(\)]+)/", $preview, $convofind);
	foreach ($convofind[2] as $ht){
		$preview=str_replace("!".$ht."","<a href='https://hey.cafe/!".$ht."'>".$ht."</a>", $preview);
	}
	
	//--Match hashtags
	$check_hash = preg_match_all("/([^&a-z0-9A-Z\/]\#)([a-z0-9A-Z\-\_]+)/", $preview, $convofind);
	foreach ($convofind[2] as $ht){
		$preview=str_replace("#".$ht."","<a href='https://hey.cafe/search/".$ht."'>#".$ht."</a>", $preview);
	}
	
	//--bold
	$check_bold = preg_match_all("/(\*\*\s)([^\n\*]+)(\s\*\*)/", $preview, $convofind);
	foreach ($convofind[2] as $ht){
		$preview=str_replace("** ".$ht." **","<strong>".$ht."</strong>",$preview);
	}
	
	//--italic
	$check_ital = preg_match_all("/(\_\_\s)([^\n\_]+)(\s\_\_)/", $preview, $convofind);
	foreach ($convofind[2] as $ht){
		$preview=str_replace("__ ".$ht." __","<i>".$ht."</i>",$preview);
	}
	
	//--sub header
	$check_subheader = preg_match_all("/(\#\#\#\s)([^\n\<\>]+)(\s\#\#\#)/", $preview, $convofind);
	foreach ($convofind[2] as $ht){
		$preview=str_replace("### ".$ht." ###","<h3>".$ht."</h3>",$preview);
	}
	
	//--header
	$check_header = preg_match_all("/(\#\#\s)([^\n\<\>]+)(\s\#\#)/", $preview, $convofind);
	foreach ($convofind[2] as $ht){
		$preview=str_replace("## ".$ht." ##","<h2>".$ht."</h2>",$preview);
	}
	
	$preview = str_replace("<p>", "<p dir=\"auto\">", $preview);
	
	return "<p dir=\"auto\">".$preview."</p>";
}