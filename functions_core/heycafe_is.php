<?php

//##############################################################
//##############################################################-- heycafe_is_system_loggedin
//##############################################################

function heycafe_is_system_loggedin($redirect=false){
	global $account_key;
	global $content_body;
	
	if ($account_key!=false){
		return true;
	}
	
	if ($redirect==true){
		$content_body.="<action type='alert'>You need to login first</action>";
		$content_body.="<action type='redirect' url='/'></action>";
	}
	return false;
}

//##############################################################
//##############################################################-- heycafe_is_system_admin
//##############################################################

function heycafe_is_system_admin($redirect=false){
	global $account_id;
	global $content_body;
	global $system_config;
	
	if ($account_id==$system_config["admin_id"]){
		return true;
	}
	
	if ($redirect==true){
		$content_body.="<action type='alert'>You do not have access to this page</action>";
		$content_body.="<action type='redirect' url='/'></action>";
	}
	return false;
}