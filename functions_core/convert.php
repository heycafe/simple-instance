<?php

//##############################################################
//##############################################################-- convert_timestring
//##############################################################

function convert_timestring($timestamp){
	$y=substr($timestamp, 0, 4); //[2018]0218105347
	$m=substr($timestamp, 4, 2); //[2018][02]18105347
	$d=substr($timestamp, 6, 2); //[2018][02][18]105347
	$h=substr($timestamp, 8, 2); //[2018][02][18][10]5347
	$mi=substr($timestamp, 10, 2); //[2018][02][18][10][53]47
	$s=substr($timestamp, 12, 2); //[2018][02][18][10][53][47]
	$stringdate="$y-$m-$d $h:$mi:$s";
	return $stringdate;
}

//##############################################################
//##############################################################-- convert_datestring
//##############################################################

function convert_datestring($timestamp){
	$y=substr($timestamp, 0, 4); //[2018]0218105347
	$m=substr($timestamp, 4, 2); //[2018][02]18105347
	$d=substr($timestamp, 6, 2); //[2018][02][18]105347
	$h=substr($timestamp, 8, 2); //[2018][02][18][10]5347
	$mi=substr($timestamp, 10, 2); //[2018][02][18][10][53]47
	$s=substr($timestamp, 12, 2); //[2018][02][18][10][53][47]
	$stringdate="$y-$m-$d";
	return $stringdate;
}

//##############################################################
//##############################################################-- convert_timeunix
//##############################################################

function convert_timeunix($timestamp){
	$y=substr($timestamp, 0, 4); //[2018]0218105347
	$m=substr($timestamp, 4, 2); //[2018][02]18105347
	$d=substr($timestamp, 6, 2); //[2018][02][18]105347
	$h=substr($timestamp, 8, 2); //[2018][02][18][10]5347
	$mi=substr($timestamp, 10, 2); //[2018][02][18][10][53]47
	$s=substr($timestamp, 12, 2); //[2018][02][18][10][53][47]
	$stringdate="$y-$m-$d $h:$mi:$s";
	return strtotime($stringdate);
}