<?php

//##############################################################
//##############################################################-- heycafe_api_request
//##############################################################

function heycafe_api_request($function,$data=[]){
	global $account_key;
	global $content_body;
	$curl = curl_init();
	$post_array=array();
	
	if ($account_key!=false){
		$post_array["auth"]=$account_key;
	}
	
	$post_array["data"]=json_encode($data);
	$post_data=http_build_query($post_array);

	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => "https://endpoint.hey.cafe/".$function."",
		CURLOPT_SSL_VERIFYPEER  => false,
		CURLOPT_USERAGENT => 'CodeSimpleScript SSC Script HTTP_REQUEST',
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_POST => true,
		CURLOPT_CONNECTTIMEOUT => 15,
		CURLOPT_TIMEOUT => 15,
		CURLOPT_POSTFIELDS => $post_data
	));
	
	// Send the request & save response
	$response = curl_exec($curl);
	
	// Close request to clear up some resources
	curl_close($curl);

	if ($response==""){
		return false;
	}else{
		return json_decode($response, true);
	}
}

//##############################################################
//##############################################################-- heycafe_api_request_admin
//##############################################################

function heycafe_api_request_admin($function,$data=[]){
	global $system_config;
	$curl = curl_init();
	$post_array=array();
	
	$post_array["auth"]=$system_config["admin_apikey"];
	$post_array["data"]=json_encode($data);
	$post_data=http_build_query($post_array);

	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => "https://endpoint.hey.cafe/".$function."",
		CURLOPT_SSL_VERIFYPEER  => false,
		CURLOPT_USERAGENT => 'CodeSimpleScript SSC Script HTTP_REQUEST',
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_POST => true,
		CURLOPT_CONNECTTIMEOUT => 15,
		CURLOPT_TIMEOUT => 15,
		CURLOPT_POSTFIELDS => $post_data
	));
	
	// Send the request & save response
	$response = curl_exec($curl);
	
	// Close request to clear up some resources
	curl_close($curl);

	if ($response==""){
		return false;
	}else{
		return json_decode($response, true);
	}
}