<?php

//##############################################################
//##############################################################-- generate_timestamp_ago
//##############################################################

function generate_timestamp_ago($timestamp,$timenow,$full=false){
	$now = new DateTime(convert_timestring($timenow));
	$ago = new DateTime(convert_timestring($timestamp));
	$diff = $now->diff($ago);
	
	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;
	
	$string = array(
	'y' => 'year',
	'm' => 'month',
	'w' => 'week',
	'd' => 'day',
	'h' => 'hour',
	'i' => 'minute',
	's' => 'second',
	);
		foreach ($string as $k => &$v){
		if ($diff->$k) {
			$v=$diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
		} else {
			unset($string[$k]);
		}
	}
	
	if ($full==false){
		$string = array_slice($string, 0, 1);
	}
	return $string ? implode(', ', $string) . ' ago' : 'just now';
}

//##############################################################
//##############################################################-- generate_readable_colour
//##############################################################

function generate_readable_colour($bg){
	$pieces = explode(",", $bg);
	$r = $pieces[0];
	$g = $pieces[1];
	$b = $pieces[2];
	
	$brightness = round((($r * 299) + ($g * 587) + ($b * 114)) / 1000);

	if($brightness > 125){
		return '0,0,0';
	}else{
		return '255,255,255';
	}
}

//##############################################################
//##############################################################-- generate_random_code
//##############################################################

function generate_random_code($length=10){
	$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}