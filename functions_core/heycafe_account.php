<?php

//##############################################################
//##############################################################-- heycafe_account_check_in_cafe
//##############################################################

function heycafe_account_check_in_cafe($account,$cafe){
	$cafe=heycafe_api_request("get_account_cafes",['query'=>$account,'cafe'=>$cafe]);
	if ($cafe["system_api_error"]==false){
		if (is_array($cafe["response_data"]["cafes"])){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}