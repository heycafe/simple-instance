<?php

set_time_limit(180);
ignore_user_abort(true);
ini_set('output_buffering', 0);
ini_set('display_errors', '0');
ini_set('date.timezone', 'Etc/GMT+8'); //--Is America/Vancouver With DTS AKA winter time in BC
ini_set('memory_limit','512M');

//##############################################################
//##############################################################-- Functions
//##############################################################

foreach (scandir("functions_core/") as $file){
	if ($file !== '.' && $file !== '..'){
		if (strpos($file, '.php') !== false){
			include("functions_core/".$file."");
		}
	}
}

foreach (scandir("functions_extra/") as $file){
	if ($file !== '.' && $file !== '..'){
		if (strpos($file, '.php') !== false){
			include("functions_extra/".$file."");
		}
	}
}

function heycafe_function($name,$data){
	if (function_exists("hc_extra_".$name."")){
		return call_user_func("hc_extra_".$name."",$data);
	}else{
		return call_user_func("hc_core_".$name."",$data);
	}
}

//##############################################################
//##############################################################-- Startup
//##############################################################

$system_page_type="404";
$system_ip=get_true_userip();
$system_useragent=makesafe($_SERVER['HTTP_USER_AGENT']);
$system_domain=makesafe($_SERVER['HTTP_HOST']);
$system_timestamp=date('YmdHis');
$system_docroot=$_SERVER["DOCUMENT_ROOT"];
$system_theme="";
$system_config="";
$system_config_update=false;

$meta_title="";
$meta_description="";
$meta_image="";
$meta_icon="";

$meta_colour_core="0, 0, 0";
$meta_colour_main="0, 0, 0";
$meta_colour_main_text="255, 255, 255";

$system_blank_image="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAoAAAAFoCAQAAABtO4LaAAADc0lEQVR42u3UMQEAAAgDINc/9IzhIYQg7QC8FAECAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQICBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEBChAQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAgIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAgAAFCAgQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQICBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBAQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEBCgAAEBAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQICFCAgQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECDAnQXyKM63O/4L6wAAAABJRU5ErkJggg==";

$content_head="";
$content_header="";
$content_breadcrum="";
$content_menu="";
$content_body="";
$content_footer="";
$content_extra="";
$content_script="";

$account_id=false;
$account_key=false;
$account_alias=false;
$account_name=false;
$account_avatar="https://b2.heycafecdn.com/file/HeyCafe/defaults/avatar_account.png";
$account_header="https://b2.heycafecdn.com/file/HeyCafe/defaults/header_account.jpg";

if (strpos($_SERVER['REQUEST_URI'], '?') !== false){
	$system_uri=makesafe(substr($_SERVER['REQUEST_URI'], 0, strrpos( $_SERVER['REQUEST_URI'], "?")));
}else{
	$system_uri=makesafe($_SERVER['REQUEST_URI']);
}
$system_uri_full=makesafe($_SERVER['REQUEST_URI']);

//##############################################################
//##############################################################-- Load files that are user editable for the settings, and the theme
//##############################################################

$system_theme=file_get_contents("".$system_docroot."/theme.html");
$system_config=json_decode(file_get_contents("".$system_docroot."/settings.hcjson"),true);

if (!file_exists("".$system_docroot."/.htaccess")){
	file_put_contents("".$system_docroot."/.htaccess", base64_decode("UmV3cml0ZUVuZ2luZSBvbgpSZXdyaXRlQ29uZCAle1JFUVVFU1RfVVJJfSAhXi9pbmRleC5waHAkClJld3JpdGVDb25kICV7UkVRVUVTVF9VUkl9ICFcLihnaWZ8anBlP2d8cG5nfGNzc3xqc3x0eHQpJApSZXdyaXRlUnVsZSAuKiAvaW5kZXgucGhwIFtOQyxMLFFTQV0"));
}

//##############################################################
//##############################################################-- Download Files
//##############################################################

if (file_exists("".$system_docroot."/javascript_core/hc_core.js")==false){ fetchurl_savefile("https://beta.hey.cafe/application/function/core.js?cache=".time()."","".$system_docroot."/javascript_core/hc_core.js"); }
if (file_exists("".$system_docroot."/javascript_core/hc_emoji.js")==false){ fetchurl_savefile("https://beta.hey.cafe/application/function/emoji.js?cache=".time()."","".$system_docroot."/javascript_core/hc_emoji.js"); }
if (file_exists("".$system_docroot."/javascript_core/hc_formating.js")==false){ fetchurl_savefile("https://beta.hey.cafe/application/function/formating.js?cache=".time()."","".$system_docroot."/javascript_core/hc_formating.js"); }
if (file_exists("".$system_docroot."/javascript_core/hc_optimizer.js")==false){ fetchurl_savefile("https://beta.hey.cafe/application/function/optimizer.js?cache=".time()."","".$system_docroot."/javascript_core/hc_optimizer.js"); }

//##############################################################
//##############################################################-- Session
//##############################################################

session_name("heycafe_si_session");
session_start();

if (!isset($_SESSION["account_key"])){
	$_SESSION["account_key"]=false;
}else{
	$account_key=$_SESSION["account_key"];
	
	//Check API key here
	$source_info_request=fetchurl("https://endpoint.hey.cafe/api/account_loginkey?query=".$account_key."");
	if ($source_info_request!=false){
		$source_info=json_decode($source_info_request,true);
		if ($source_info["system_api_error"]!="notvalid"){
			if (isset($source_info["response_data"]["id"])){
				if ($source_info["response_data"]["auth_domain"]==$system_domain){
					$account_id=$source_info["response_data"]["id"];
					$account_alias=$source_info["response_data"]["alias"];
					$account_name=$source_info["response_data"]["name"];
					$account_avatar=$source_info["response_data"]["avatar"];
					$account_header=$source_info["response_data"]["header"];
				}
			}
		}
	}
}

//--Did not get session data so clear it
if ($account_id==false){
	$_SESSION["account_key"]=false;
	$account_key=false;
}

//##############################################################
//##############################################################-- Compile Scripts
//##############################################################

if (file_exists("script_combined_compressed.js")==false){
	ob_start();
	
	foreach (scandir("javascript_core/") as $file){
		if ($file !== '.' && $file !== '..'){
			if (strpos($file, '.js') !== false){
				include("javascript_core/".$file."");
			}
		}
	}
	
	foreach (scandir("javascript_extra/") as $file){
		if ($file !== '.' && $file !== '..'){
			if (strpos($file, '.js') !== false){
				include("javascript_extra/".$file."");
			}
		}
	}
	
	$output = ob_get_contents(); //Gives whatever has been "saved"
	ob_end_clean(); //Stops saving things and discards whatever was saved
	ob_flush();
	
	$file_mini = fopen(getenv("DOCUMENT_ROOT") . "/script_combined_compressed.js","w");
	fwrite($file_mini,compress($output));
	fclose($file_mini);
}

$contents = file_get_contents("./script_combined_compressed.js", FILE_USE_INCLUDE_PATH);
$content_script="<script>".$contents."</script>";

//##############################################################
//##############################################################-- Meta Details
//##############################################################

$meta_title=$system_config["meta_title"];
$meta_description=$system_config["meta_description"];
$meta_image=$system_config["meta_image"];
$meta_icon=$system_config["meta_icon"];
$meta_colour_core=$system_config["meta_colour_core"];
$meta_colour_main=$system_config["meta_colour_main"];
$meta_colour_main_text=$system_config["meta_colour_main_text"];

//##############################################################
//##############################################################-- Body Content
//##############################################################

//--Load generators based on content area
if ($system_uri=="/"){
	include("app/index.php");
}else{
	if (file_exists("app/".trim($system_uri,"/").".php")){
		include("app/".trim($system_uri,"/").".php");
	}else{
		if (file_exists("app/".trim($system_uri,"/")."/index.php")){
			include("app/".trim($system_uri,"/")."/index.php");
		}else{
			$subpagecheck=substr(trim($system_uri,"/"), 0, strrpos(trim($system_uri,"/"), '/'));
			if (file_exists("app/".trim($subpagecheck,"/")."/subpage.php")){
				include("app/".trim($subpagecheck,"/")."/subpage.php");
			}else{
				include("app/404.php");
			}
		}
	}
}

if (str_starts_with($system_uri,"/.well-known")){
	if (file_exists("".$system_docroot."/".trim($system_uri,"/")."")){
		$system_theme=file_get_contents("".$system_docroot."/".trim($system_uri,"/")."");
	}else{
		$content_body="Error 404";
	}
}

//##############################################################
//##############################################################-- Header Content
//##############################################################
	
//--Logged IN
if ($account_id!=false){
	$content_header.="<a href='/'><img class='logo' alt='Your avatar' src='".$account_avatar."'></a>";
	$content_header.="<div class='nav'>";
	$content_header.="<a href='/feed'>Feed</a>";
	$content_header.="<a href='/profile/".$account_alias."'>Profile</a>";
	if ($account_id==$system_config["admin_id"]){
		$content_header.="<a href='/setup'>Setup</a>";
	}
	$content_header.="<a href='/logout' class='rightbreak important norightspace'>Logout</a>";
	$content_header.="</div>";
}

//--NOT Logged in
if ($account_id==false){
	$content_header.="<a href='/'><img class='logo' alt='Our Logo' src='".$meta_icon."'></a>";
	$content_header.="<div class='nav'>";
	$content_header.="<a href='/about'>About</a>";
	$content_header.="<a href='/register' class='rightbreak'>Register</a>";
	$content_header.="<a href='/login' class='important norightspace'>Login</a>";
	$content_header.="</div>";
}

//--Get site title based on a few factors
$breadcrum_parent_title="";
$breadcrum_parent_subtitle="";
preg_match_all("/\>([^\<\>]*)\<\/heading\>/", $content_body, $hvalues, PREG_SET_ORDER);
foreach ($hvalues as $ht){
	if ($breadcrum_parent_title==""){
		$breadcrum_parent_title=$ht[1];
	}
}
preg_match_all("/\>([^\<\>]*)\<\/subheading\>/", $content_body, $hvalues, PREG_SET_ORDER);
foreach ($hvalues as $ht){
	if ($breadcrum_parent_title==""){
		$breadcrum_parent_title=$ht[1];
	}
}

if ($breadcrum_parent_title!=""){
	$meta_title="".makesafe($breadcrum_parent_title)." / ".$meta_title."";
}

//##############################################################
//##############################################################-- Footer Content
//##############################################################

$content_footer.='<footer><div style="font-size:0.8em;">'.$system_config["content_footer"].'</div></footer>';

//##############################################################
//##############################################################-- Save Updates
//##############################################################

if ($system_config_update==true){
	file_put_contents("".$system_docroot."/settings.hcjson", json_encode($system_config, JSON_PRETTY_PRINT));
}

//##############################################################
//##############################################################-- Print out page
//##############################################################

$system_theme=str_replace("{{content_head}}", $content_head, $system_theme);
$system_theme=str_replace("{{content_header}}", $content_header, $system_theme);
$system_theme=str_replace("{{content_menu}}", $content_menu, $system_theme);
$system_theme=str_replace("{{content_body}}", $content_body, $system_theme);
$system_theme=str_replace("{{content_footer}}", $content_footer, $system_theme);
$system_theme=str_replace("{{content_extra}}", $content_extra, $system_theme);
$system_theme=str_replace("{{content_script}}", $content_script, $system_theme);

$system_theme=str_replace("{{meta_title}}", $meta_title, $system_theme);
$system_theme=str_replace("{{meta_description}}", $meta_description, $system_theme);
$system_theme=str_replace("{{meta_image}}", $meta_image, $system_theme);
$system_theme=str_replace("{{meta_icon}}", $meta_icon, $system_theme);

$system_theme=str_replace("{{meta_colour_core}}", $meta_colour_core, $system_theme);
$system_theme=str_replace("{{meta_colour_main}}", $meta_colour_main, $system_theme);
$system_theme=str_replace("{{meta_colour_main_text}}", $meta_colour_main_text, $system_theme);

$system_theme=str_replace("{{cache}}", $system_timestamp, $system_theme);
$system_theme=str_replace("{{blankimage}}", $system_blank_image, $system_theme);

echo $system_theme;