var current_page="" + window.location.pathname + "" + window.location.search + "";
var current_url=window.location.href;
var current_title=document.title;
var current_domain=window.location.hostname;
var is_page_loading=false;

if (window.location.protocol == "http:"){
	console.log("You are not connected with a secure connection.")
	console.log("Reloading the page to a Secure Connection...")
	window.location = document.URL.replace("http://", "https://");
}

if (window.location.protocol == "https:"){
	console.log("You are connected with a secure connection.")
}

//--#############################################################
//--############################################################# -- getHTTP
//--#############################################################

function getHTTP(url, callback, maxtime){
	var xhr = new XMLHttpRequest();
	var timeout=10000;
	if (maxtime!=undefined){
		timeout=maxtime;
	}
	
	xhr.timeout = timeout;
	xhr.withCredentials = true;
	
	xhr.onreadystatechange = function(){
		if(xhr.readyState==4 && xhr.status==200){
			content = xhr.responseText;
			callback(content);
		}else{
			if(xhr.readyState==4 && xhr.status!=200){
				callback("{ \"fail\": \"unknown\", \"system_api_error\": true }");
			}
		}
		
		var percent=1;
		if(xhr.readyState==1){
			percent=15;
		}
		if(xhr.readyState==2){
			percent=50;
		}
		if(xhr.readyState==3){
			percent=75;
		}
		if(xhr.readyState==4){
			percent=100;
		}
		
		if (document.getElementById("page_loader_progress")){
			document.getElementById("page_loader_progress").innerHTML="<div class='loading_progress_case'><div class='loading_progress_inside' style='width:" + percent + "%;'></div></div>";
		}
	}
	
	xhr.ontimeout = function() {
		callback("{ \"fail\": \"timeout\", \"system_api_error\": true }");
	}
	xhr.open("GET", url, true);
	
	xhr.send();
}

//--#############################################################
//--############################################################# -- postHTTP
//--#############################################################

function postHTTP(url, data, callback){
  var queryString = "";
  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;
  xhr.onreadystatechange = function(){
	if(xhr.readyState==4 && xhr.status==200){
	  content = xhr.responseText;
	  callback(content);
	}
	
	var percent=5;
	if(xhr.readyState==1){
		percent=15;
	}
	if(xhr.readyState==2){
		percent=50;
	}
	if(xhr.readyState==3){
		percent=75;
	}
	if(xhr.readyState==4){
		percent=99;
	}
	
	if (document.getElementById("page_loader_progress")){
		document.getElementById("page_loader_progress").innerHTML="<div class='loading_progress_case'><div class='loading_progress_inside' style='width:" + percent + "%;'></div></div>";
	}
  }
  
  xhr.open("POST", url, true);
  var queryString = data;
  
  xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  xhr.send(queryString);
}

//--#############################################################
//--############################################################# -- postForm
//--#############################################################

function postForm(form){
	console.log(form);
	
	let data = new FormData();
	var url=current_page;
	var quiet=false;
	var element=false;
	
	if (form.hasAttribute("url")){
		url=form.getAttribute("url");
	}
	
	if (form.hasAttribute("element")){
		element=form.getAttribute("element");
	}
	
	if (form.hasAttribute("quiet")){
		if (form.getAttribute("quiet")=="true"){
			quiet=true;
		}
	}
	
	if (form.getElementsByTagName("INPUT")){
		runinputs = form.getElementsByTagName("INPUT");
		[].slice.call(runinputs).forEach(function(item){
			var type=item.getAttribute("type");
			var name=item.getAttribute("name");
			
			if (type=="text"){
				var value=item.value;
				data.append(name, value);
			}
			if (type=="password"){
				var value=item.value;
				data.append(name, value);
			}
		});
	}
	
	if (form.getElementsByTagName("SELECT")){
		runinputs = form.getElementsByTagName("SELECT");
		[].slice.call(runinputs).forEach(function(item){
			var type=item.getAttribute("type");
			var name=item.getAttribute("name");
			if (type=="text"){
				var value=item.value;
				data.append(name, value);
			}
			
			if (type=="dropdown"){
				var value=item.options[item.selectedIndex].value;
				data.append(name, value);
			}
		});
	}
	
	if (form.getElementsByTagName("TEXTAREA")){
		runinputs = form.getElementsByTagName("TEXTAREA");
		[].slice.call(runinputs).forEach(function(item){
			var type=item.getAttribute("type");
			var name=item.getAttribute("name");
			var encode=false;
			
			if (item.hasAttribute("encode")){
				if (item.getAttribute("encode")=="true"){
					encode=true;
				}
			}
			
			if (type=="text"){
				var value=item.value;
				if (encode==true){
					value=b64EncodeUnicode(value);
				}
				data.append(name, value);
			}
		});
	}
	
	if (quiet==false){
		switchPage_loading_set();
		is_page_loading=true;
	}else{
		document.getElementById("" + element + "").innerHTML="Sending...";
	}
	
	let request = new XMLHttpRequest();
	request.withCredentials = true;
	request.open('POST', url);
	
	request.addEventListener('load', function(e){
		var status=request.status;
		var result=request.response;
		if (quiet==false){
			switchPage_loading_remove();
			switchPage_replace(url,result);
			is_page_loading=false;
		}else{
			inlinePage_element_replace(url,element,result);
		}
		document.body.scrollTop = document.documentElement.scrollTop = 0;
		backgroundchecks(false);
	});
	
	//--send POST request to server side script
	request.send(data);
}

function postFile(element,preview){
  var uploadgood=true;
  var uploadmessage="";
  var file=element.files[0];
  var size=element.files[0].size;
  var size_kb=size/1024;
  var size_mb=size_kb/1024;
  var url=current_page;
  
  if (element.hasAttribute("url")){
	  url=element.getAttribute("url");
  }
  
  //--Make needed elements
  document.getElementById(preview).innerHTML="<div style='text-align:center;margin-top:20px;margin-bottom:20px;'><div id='uploader_temp_header'><strong>Uploading file...</strong><BR><div class='loading'></div></div><div id='uploader_temp_progress'></div></div>";
  
  //--File size
  if (size_mb>=96){
	uploadgood=false;
	uploadmessage="File is to large, file needs to be 96mb or under";
  }
  
  //--Preview casing
  if (document.getElementById(preview)==undefined){
	uploadgood=false;
	uploadmessage="Upload area missing";
  }
  
  //--START UPLOADER HERE!
  if (uploadgood==true){
	let data = new FormData();
	data.append('file', file);
	
	let request = new XMLHttpRequest();
	request.withCredentials = true;
	request.open('POST', url);
	
	request.upload.addEventListener('progress', function(e) {
	let percent_complete = (e.loaded / e.total)*100;
	  document.getElementById("uploader_temp_progress").innerHTML="" + Math.round(percent_complete) + "%";
	});
	
	request.addEventListener('load', function(e){
	  var status=request.status;
	  var result=request.response;
	  
	  switchPage_replace(url,result);
	  document.body.scrollTop = document.documentElement.scrollTop = 0;
	  is_page_loading=false;
	  backgroundchecks(false);
	});
	
	//--send POST request to server side script
	request.send(data);
  }else{
	document.getElementById("uploader_temp_header").innerHTML="<strong>Error</strong>";
	document.getElementById("uploader_temp_progress").innerHTML=uploadmessage;
  }

}

//--#############################################################
//--############################################################# -- Event DOMContentLoaded
//--#############################################################

document.addEventListener("DOMContentLoaded", function(){
	backgroundchecks(true);
	optimizerLazyScrollCheck();
	setTimeout("optimizerLazyScrollCheck();", 200);
});

function backgroundchecks(repeat){
	if (repeat==undefined){
		repeat=false;
	}
	
	//--Emoji convert
	if (document.getElementsByClassName("emojiconvert")){
		runemojies = document.getElementsByClassName("emojiconvert");
		[].slice.call(runemojies).forEach(function(item){
		  var source=item.getAttribute("alt");
		  source=b64DecodeUnicode(source);
		  item.setAttribute('alt', source);
		  item.classList.remove("emojiconvert");
		});
	}
	
	//--Special Inputs work
	if (document.getElementsByTagName("INPUT")){
		runinputs = document.getElementsByTagName("INPUT");
		[].slice.call(runinputs).forEach(function(item){
			var type=item.getAttribute("type");
			
			if (type=="submit"){
				if (item.hasAttribute("onclick")==false){
					item.setAttribute('onclick', "postForm(this.form); return false;");
				}
			}
			
		});
	}
	
	if (document.getElementsByTagName("TEXTAREA")){
		runactions = document.getElementsByTagName("TEXTAREA");
		[].slice.call(runactions).forEach(function(item){
			if (item.hasAttribute("activated")==false){
				
				if (item.hasAttribute("encoded")){
					item.value=b64DecodeUnicode(item.value);
				}
				
				item.setAttribute('style', 'height:' + (item.scrollHeight) + 'px;overflow:hidden;');
				item.setAttribute('activated', 'true');
				
				setTimeout(function(){
					item.style.height = (item.scrollHeight) + 'px';
				}, 10);
			}else{
				if (item.offsetHeight<=(item.scrollHeight)){
					item.style.height = (item.scrollHeight) + 'px';
				}
			}
		});
	}
	
	//--Action elements (things for us to do!)
	if (document.getElementsByTagName("ACTION")){
		runactions = document.getElementsByTagName("ACTION");
		[].slice.call(runactions).forEach(function(item){
			var done=false;
			var type=item.getAttribute("type");
			
			if (type=="format_emoji"){
				var element=item.getAttribute("element");
				if (document.getElementById(element)){
					var contents=document.getElementById(element).innerHTML;
					contents=emojiReplace(contents);
					document.getElementById(element).innerHTML=contents;
					optimizerLazyScrollCheck();
				}
				done=true;
			}
			
			if (type=="format_basic"){
				var element=item.getAttribute("element");
				if (document.getElementById(element)){
					var contents=document.getElementById(element).innerHTML;
					contents=formatBasicText(contents);
					contents=emojiReplace(contents);
					document.getElementById(element).innerHTML=contents;
					optimizerLazyScrollCheck();
				}
				done=true;
			}
			
			if (type=="format_simple"){
				var element=item.getAttribute("element");
				if (document.getElementById(element)){
					var contents=document.getElementById(element).innerHTML;
					contents=formatBasicText(contents);
					contents=formatBasicLinks(contents);
					contents=emojiReplace(contents);
					contents=formatInternalLinksExt(contents);
					document.getElementById(element).innerHTML=contents;
					optimizerLazyScrollCheck();
				}
				done=true;
			}
			
			if (type=="heycafe_embed"){
				var element=item.getAttribute("element");
				heycafeEmbedScript(element);
				done=true;
			}
			
			if (type=="inview_load_preview_conversation"){
				var send_element=item.getAttribute("element");
				var send_conversation=item.getAttribute("conversation");
				var embed='<iframe frameborder="0" style="width:100%;height:80px;margin:0px auto;overflow:hidden;background:transparent;display:block;opacity: 0;" onload="this.style.opacity = 1;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts" src="https://beta.hey.cafe/embed/conversation/' + send_conversation + '" id="heycafe_conversation_' + send_conversation + '"></iframe>';
				document.getElementById(send_element).innerHTML=embed;
				heycafeEmbedScript('heycafe_conversation_' + send_conversation + '');
				done=true;
			}
			
			if (type=="inview_load_preview_comment"){
				var send_element=item.getAttribute("element");
				var send_conversation=item.getAttribute("conversation");
				var send_comment=item.getAttribute("comment");
				var embed='<iframe frameborder="0" style="width:100%;height:80px;margin:0px auto;overflow:hidden;background:transparent;display:block;opacity: 0;" onload="this.style.opacity = 1;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts" src="https://beta.hey.cafe/embed/comment/' + send_comment + '" id="heycafe_comment_' + send_comment + '"></iframe>';
				document.getElementById(send_element).innerHTML=embed;
				heycafeEmbedScript('heycafe_comment_' + send_comment + '');
				done=true;
			}
			
			if (type=="heycafe_sign_in_with"){
				var element=item.getAttribute("element");
				var publisher=item.getAttribute("publisher");
				var reason=item.getAttribute("reason");
				var url=item.getAttribute("url");
				var title=item.getAttribute("title");
				var newelem=document.createElement('script');
				newelem.src='https://beta.hey.cafe/application/external/sign_in_with.js?v=' + randomString(10);
				newelem.setAttribute('publisher',publisher);
				newelem.setAttribute('reason',reason);
				newelem.setAttribute('url',url);
				newelem.setAttribute('title',title);
				newelem.setAttribute('request','login');
				newelem.setAttribute('element',"" + element + "_child");
				document.getElementById(element).innerHTML="<div id='" + element + "_child'><div class='sign_in_with_hey'><div class='case' style='background: rgb(96, 79, 216);color:rgb(255,255,255);border-radius:10px;padding:10px;padding-left:15px;padding-right:15px;text-align:center;font-weight: 700;max-width: 400px;margin: 0px auto;margin-bottom: 15px;margin-top: 15px;'><img class='image' src='https://hey.cafe/logo.png' style='margin-right:15px;float:left;max-height: 22px;'> Loading...</div></div></div>";
				document.getElementById(element).appendChild(newelem);
				done=true;
			}
			
			if (type=="heycafe_sign_in_with_api"){
				var element=item.getAttribute("element");
				var publisher=item.getAttribute("publisher");
				var reason=item.getAttribute("reason");
				var url=item.getAttribute("url");
				var title=item.getAttribute("title");
				var newelem=document.createElement('script');
				newelem.src='https://beta.hey.cafe/application/external/sign_in_with.js?v=' + randomString(10);
				newelem.setAttribute('publisher',publisher);
				newelem.setAttribute('reason',reason);
				newelem.setAttribute('url',url);
				newelem.setAttribute('title',title);
				newelem.setAttribute('request','api');
				newelem.setAttribute('element',"" + element + "_child");
				document.getElementById(element).innerHTML="<div id='" + element + "_child'><div class='sign_in_with_hey'><div class='case' style='background: rgb(96, 79, 216);color:rgb(255,255,255);border-radius:10px;padding:10px;padding-left:15px;padding-right:15px;text-align:center;font-weight: 700;max-width: 400px;margin: 0px auto;margin-bottom: 15px;margin-top: 15px;'><img class='image' src='https://hey.cafe/logo.png' style='margin-right:15px;float:left;max-height: 22px;'> Loading...</div></div></div>";
				document.getElementById(element).appendChild(newelem);
				done=true;
			}
			
			if (type=="format_complex"){
				var element=item.getAttribute("element");
				if (document.getElementById(element)){
					var contents=document.getElementById(element).innerHTML;
					contents=formatComplexText(contents);
					contents=formatComplexLinks(contents);
					contents=emojiReplace(contents);
					contents=formatLargerEmoji(contents);
					contents=formatCompexFixes(contents);
					contents=formatInternalLinksExt(contents);
					document.getElementById(element).innerHTML=contents;
					afterloadRun(contents);
					
					optimizerLazyScrollCheck();
				}
				done=true;
			}
			
			if (type=="inview_load_metadata"){
				var send_element=item.getAttribute("element");
				var send_url=item.getAttribute("url");
				metadata(send_element,send_url);
				done=true;
			}
			
			if (type=="inline_load"){
				var send_url=item.getAttribute("url");
				var send_element=item.getAttribute("element");
				var send_append=item.getAttribute("append");
				setTimeout(function(){
					inlinePage_element(send_url,send_element,send_append);
				}, 100);
				//--document.location.href = send_url;
				done=true;
			}
			
			if (type=="redirect"){
				var send_url=item.getAttribute("url");
				switchPage(send_url);
				//--document.location.href = send_url;
				done=true;
			}
			
			if (type=="redirect_delay"){
				var send_url=item.getAttribute("url");
				setTimeout(function(){
					switchPage(send_url);
				}, 2000);
				done=true;
			}
			
			if (type=="redirect_reload"){
				var send_url=item.getAttribute("url");
				document.location.href = send_url;
				done=true;
			}
			
			if (type=="redirect_newtab"){
				var send_url=item.getAttribute("url");
				window.open(send_url, '_blank').focus();
				done=true;
			}
			
			if (type=="set_title"){
				var send_title=item.getAttribute("title");
				document.title=send_title;
				done=true;
			}
			
			if (type=="alert"){
				var contents=item.innerHTML;
				uiAlert(contents);
				done=true;
			}
			
			if (done==true){
				item.remove();
			}
		});
	}
	
	if (repeat==true){
		setTimeout("backgroundchecks(true);", 200);
	}
}

//--#############################################################
//--############################################################# -- uiAlert
//--#############################################################

function uiAlert(message){
  notificationid="notification_" + randomString(30) + "";
  document.getElementById("uiAlert").innerHTML="<div onclick=\"uiAlert_clear('" + notificationid + "');\" id='" + notificationid + "' class='notification hide'>" + message + "</div>";
  
  setTimeout(function(){
	if (document.getElementById(notificationid)!=undefined){
	  document.getElementById(notificationid).classList.remove("hide");
	  document.getElementById(notificationid).classList.add("show");
	}
  }, 100);
  
  setTimeout("uiAlert_clear('" + notificationid + "');", 5000);
}

function uiAlert_clear(id){
  if (document.getElementById(id)!=undefined){
	document.getElementById(id).classList.remove("show");
	document.getElementById(id).classList.add("hide");
	setTimeout(function(){
	  if (document.getElementById(id)!=undefined){
		document.getElementById(id).remove();
	  }
	}, 1000);
  }
}

//--#############################################################
//--############################################################# -- addEventListener Click
//--#############################################################

document.addEventListener('click', function (event){
	optimizerLazyScrollCheck();
	var target=event.target;
	
	if (target!=null){ if (target.hasAttribute("href")==false){ target=target.parentElement; }}
	if (target!=null){ if (target.hasAttribute("href")==false){ target=target.parentElement; }}
	if (target!=null){ if (target.hasAttribute("href")==false){ target=target.parentElement; }}
	if (target!=null){ if (target.hasAttribute("href")==false){ target=target.parentElement; }}
	if (target!=null){ if (target.hasAttribute("href")==false){ target=target.parentElement; }}
	if (target!=null){ if (target.hasAttribute("href")==false){ target=target.parentElement; }}
	if (target!=null){ if (target.hasAttribute("href")==false){ target=target.parentElement; }}
	if (target!=null){ if (target.hasAttribute("href")==false){ target=target.parentElement; }}
	if (target!=null){ if (target.hasAttribute("href")==false){ target=target.parentElement; }}
	if (target!=null){ if (target.hasAttribute("href")==false){ target=target.parentElement; }}
	if (target!=null){ if (target.hasAttribute("href")==false){ target=target.parentElement; }}
	
	//--Change URL
	if (target!=null){
		if (target.hasAttribute("href")){
			var url=target.getAttribute('href');
			if (!url.match(/https:/) && !url.match(/http:/)){
				if (!url.match(/javascript:/)){
					if (!url.match(/mailto:/)){
						if (!url.match(/sftp:/)){
							event.preventDefault();
							event.stopPropagation();
							switchPage(url);
						}
					}
				}
			}else{
				if (!url.match(/hey\.cafe\/request/)){
					event.preventDefault();
					window.open(url, '_blank');
				}
			}
		}
	}
}, false);

//--#############################################################
//--############################################################# -- addEventListener Scroll
//--#############################################################

document.addEventListener('scroll', function(e){
	optimizerLazyScrollCheck();
});

//--#############################################################
//--############################################################# -- addEventListener popstate
//--#############################################################

window.addEventListener('popstate', function (event){
	if (current_page!="" + window.location.pathname + "" + window.location.search + ""){
		switchPage("" + window.location.pathname + "" + window.location.search + "");
	}
});

//--#############################################################
//--############################################################# -- switchPage
//--#############################################################

function switchPage(url){
	console.log("Switch page to " + url + "");
	switchPage_loading_set();
	if (!url.match(/https:/) && !url.match(/http:/)){
		is_page_loading=true;
		getHTTP(url,function(result){
			switchPage_loading_remove();
			switchPage_replace(url,result);
			document.body.scrollTop = document.documentElement.scrollTop = 0;
			is_page_loading=false;
			backgroundchecks(false);
		});
	}else{
		window.location.href = url;
	}
}

function switchPage_loading_set(){
	document.getElementById("js_wrap_floatbox").innerHTML="<div style='opacity:0.6;'>" + document.getElementById("js_wrap_floatbox").innerHTML + "</div>";
	document.getElementById("js_wrap_footer").innerHTML="<div style='opacity:0.6;'>" + document.getElementById("js_wrap_footer").innerHTML + "</div>";
	if (document.getElementById("js_wrap_menu").innerHTML!=""){
		document.getElementById("js_wrap_menu").innerHTML="<div style='opacity:0.6;'>" + document.getElementById("js_wrap_menu").innerHTML + "</div>";
	}
	
	document.getElementById("js_wrap_floatbox").innerHTML="<div style='margin-top:50px;margin-bottom:50px;'><div class='loading'></div></div><div id='page_loader_progress'></div>";
	//--document.getElementById("uiLoading").innerHTML="<div style='position: fixed;width: 100%;margin-top: 200px;'><div class='loading'></div><div id='page_loader_progress'></div></div>";
}

function switchPage_loading_remove(){
	document.getElementById("js_wrap_floatbox").innerHTML="";
	document.getElementById("js_wrap_footer").innerHTML="";
	document.getElementById("js_wrap_menu").innerHTML="";
	
	document.getElementById("uiLoading").innerHTML="";
}

function switchPage_replace(url,result){
	var doc = new DOMParser().parseFromString(result, "text/html");
	if (doc.getElementById("js_copy_content")!=undefined){
		current_page=url;
		var current_title=document.title;
		if (doc.getElementsByTagName("TITLE")[0]!=undefined){
			current_title = decodeHtml(doc.getElementsByTagName("TITLE")[0].innerHTML);
		}
		document.title=current_title;
		current_url="https://" + window.location.hostname + "" + url;
		window.history.pushState({"html":url,"pageTitle":"" + current_title + ""},"", "https://" + window.location.hostname + "" + url);
		var data = doc.getElementById("js_copy_content").innerHTML;
		document.getElementById("js_copy_content").innerHTML="";
		document.getElementById("js_copy_content").insertAdjacentHTML('beforeend', data);
	}else{
		document.getElementById("js_wrap_floatbox").innerHTML="<heading>Not found</heading><bubble class='warning'><h3 style='margin:0px;'>Sorry we cant find the page, do you want to go back?<BR><BR><a href=\"javascript:switchPage('" + current_page + "');\" class=\"button\">Back</a></h3></bubble>";
	}
	setTimeout("optimizerLazyScrollCheck();", 200);
	setTimeout("optimizerLazyScrollCheck();", 400);
}

//--#############################################################
//--############################################################# -- inlinePage_element
//--#############################################################

function inlinePage_element(url,element,appendloader){
	if (document.getElementById("" + element + "")){
		if (appendloader=="true"){
			var oldpage=document.getElementById("" + element + "").innerHTML;
		}else{
			var oldpage="";
		}
		document.getElementById("" + element + "").innerHTML="" + oldpage + "<div class='loading'></div>";
		getHTTP(url,function(result){
			inlinePage_element_replace(url,element,result);
			backgroundchecks(false);
		},90000);
	}
}

function inlinePage_element_replace(url,element,result){
	var doc = new DOMParser().parseFromString(result, "text/html");
	if (doc.getElementById("js_copy_content")!=undefined){
		var data = doc.getElementById("js_wrap_floatbox").innerHTML;
		if (doc.getElementById("js_inline_replace")){
			var data=doc.getElementById("js_inline_replace").innerHTML;
		}
		if (document.getElementById("" + element + "")){
			document.getElementById("" + element + "").innerHTML="";
			document.getElementById("" + element + "").insertAdjacentHTML('beforeend', data);
		}
	}else{
		document.getElementById("" + element + "").innerHTML="Content load failed...";
	}
}

//--#############################################################
//--############################################################# -- metadata
//--#############################################################

function metadata(element,url){
	if (document.getElementById(element)){
		getHTTP("https://endpoint.hey.cafe/api/website_meta?query=" + b64EncodeUnicode(url) + "",function(result){
			var response = decodeJSON(result);
			if (response["fail"]==undefined){
			  if (response["system_api_error"]==false){
				var data=response["response_data"];
				var view="preview";
				var preview_noinfo=false;
				document.getElementById(element).innerHTML="";
				
				if (url.endsWith(".jpeg") || url.endsWith(".jpg") || url.endsWith(".png") || url.endsWith(".gif") || url.endsWith(".webp")){ view="image"; }
				if (url.includes("youtube.com") || url.includes("youtu.be")){ if (url.includes("/watch?") || url.includes("embed/") || url.includes("youtu.be/")){ view="youtube_embed"; }}
				
				//--Check if the preview has no info
				if (data["title"]=="false"){
					if (data["description"]=="false"){
						preview_noinfo=true;
					}
				}
				
				//--Save OG info
				var org_description=data["description"];
				var org_title=emojiReplace(data["title"]);
				
				//--User facing preview URL
				var url_preview_text=data["url"];
				url_preview_text=url_preview_text.replace("https://", "");
				url_preview_text=url_preview_text.replace("http://", "");
				url_preview_text=url_preview_text.replace("www.", "");
				
				if (data["title"]=="false"){ data["title"]="No title found..."; }else{ data["title"]=parseHtmlEntities(Array.from(data["title"]).slice(0, 110).join('')); if (data["title"].length>=108){ data["title"]="" + data["title"] + "..." } }
				if (data["description"]=="false"){ data["description"]="No description..."; }else{ data["description"]=parseHtmlEntities(Array.from(data["description"]).slice(0, 220).join('')); if (data["description"].length>=218){ data["description"]="" + data["description"] + "..." } }
				
				//--Formatting
				data["title"]=emojiReplace(data["title"]);
				data["description"]=removeLinks(data["description"]);
				data["description"]=emojiReplace(data["description"]);
				
				//--See if there is an image for the site
				var image="";
				if (data["image"]!="false"){
					if (data["image"].includes("http")==false){
						if (data["image"].includes("//")==false){
							var hostname=getHostnameFromString(url);
							if (data["image"].charAt(0)=="/"){
								data["image"]="http://" + hostname + "" + data["image"] + "";
							}else{
								data["image"]="http://" + hostname + "/" + data["image"] + "";
							}
						}
					}
				}
				
				if (data["image"]!="false"){
					if (window.innerWidth>=500){
						image="<div class='media_backing optimizer_bg aspectcase sixteenbynine' source='" + data["image"] + "' style=\"background-image: url('" + settings_blank_image + "');background-size: cover;\" size='6'><div class='media_bg content optimizer_bg' source='" + data["image"] + "' style=\"background-image: url('" + settings_blank_image + "');background-size: contain;background-position: center center;background-repeat: no-repeat;\"></div></div>";
					}
				}
				
				//--VIEW IMAGE
				if (view=="image"){
					document.getElementById(element).innerHTML="<div class='media_backing optimizer_bg aspectcase sixteenbynine' source='" + url + "' style=\"background-image: url('" + settings_blank_image + "');background-size: cover;\" size='6'><div class='media_bg content optimizer_bg' source='" + url + "' style=\"background-image: url('" + settings_blank_image + "');background-size: contain;background-position: center center;background-repeat: no-repeat;\"></div></div>";
				}
				
				//--VIEW YOUTUBE EMBED
				if (view=="youtube_embed"){
					var foundid=url.match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/);
					var videoid=(foundid&&foundid[7].length==11)? foundid[7] : false;
					
					if (videoid==false){
					  var foundidtwo=url.match(/^.*youtu.be\/([^#&?]*).*/);
					  var videoid=(foundidtwo&&foundidtwo[1].length==11)? foundidtwo[1] : false;
					}
					
					if (videoid!=false){
						document.getElementById(element).innerHTML='<div class="aspectcase sixteenbynine"><div class="content"><iframe allow="autoplay *; encrypted-media *; fullscreen *" style="width:100%;height:100%;border:0px;border-radius:6px;" src="https://www.youtube.com/embed/' + videoid + '"></iframe></div></div>';
					}
				}
				
				//--VIEW PREVIEW
				if (view=="preview"){
					if (preview_noinfo==false){
						document.getElementById(element).innerHTML="<div class='embed'><div class='preview'>" + image + "</div><a href='" + url + "' target='_blank' rel='noreferrer'><strong>" + data["title"] + "</strong></a><p>" + data["description"] + "</p><div style='opacity:0.7;font-weight: 500;font-size: 0.8em;overflow:hidden; white-space:nowrap;text-overflow: ellipsis;'><a href='" + url + "' target='_blank' rel='noreferrer'>" + url_preview_text + "</a></div></div>";
					}else{
						document.getElementById(element).innerHTML="<p><a href='" + url + "' target='_blank' rel='noreferrer'>" + url_preview_text + "</a></p>";
					}
				}
				
				setTimeout("optimizerLazyScrollCheck();", 200);
				setTimeout("optimizerLazyScrollCheck();", 400);
			  }
			}else{
			  setTimeout(function(){
				generateMetadata_load(element,url);
			  }, 3000);
			}
		});
	}
}

function decodeHtml(html){
	var txt = document.createElement("textarea");
	txt.innerHTML = html;
	return txt.value;
}

function afterloadRun(contents){
	var matches = contents.match(/\<attachment id=\'file\_([^\n\']+)\'\>/gi);
	forEach(matches, function(id){
		var data=matches["" + id + ""];
		var result = data.match(/id=\'file\_([^\n\']+)\'/);
		var filename=result[1];
		if (document.getElementById("bottom_file_" + filename + "")){
			document.getElementById("file_" + filename + "").innerHTML=document.getElementById("bottom_file_" + filename + "").innerHTML;
			document.getElementById("bottom_file_" + filename + "").innerHTML="";
		}
	});
}

function heycafeEmbedScript(id){
	if (document.getElementById(id)){
		if (document.getElementById("script_hc_" + id + "")){
			var deletemescript=document.getElementById("script_hc_" + id + "");
			deletemescript.parentNode.removeChild(deletemescript);
		}
		var element=document.createElement('script');
		element.id="script_hc_" + id + "";
		element.text = '{ let hey_framename="' + id + '"; let hey_iframe = document.querySelector("#" + hey_framename); window.addEventListener(\'message\', function(e){ if (e.source == hey_iframe.contentWindow){ if (e.origin.includes("hey.cafe")){ let hey_message = e.data; hey_iframe.style.height = hey_message.height + \'px\'; hey_offset=document.documentElement.scrollTop-hey_iframe.offsetTop; if (hey_offset<=0){ hey_offset=0; } let hey_response = { type: "embed_position", value: hey_offset }; e.source.postMessage(hey_response, e.origin); }}} , false); }';
		document.body.appendChild(element);
	}else{
		setTimeout("heycafeEmbedScript('" + id + "')", 200);
	}
}